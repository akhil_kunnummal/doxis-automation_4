
const data2 = require('../data/URL.json');
const data = require('../data/TestData.json');
//'const XLSX = require('xlsx');'
const fs = require('fs');
//const Excel = require('exceljs');
const { Console } = require('console');

const searchbox_text='//*[@id="i6f2af081-359f-11ec-8b59-cd14cba38ed9"]';
const date_parm='//button[@class="euiSuperDatePicker__prettyFormat"]'
const data_parm_2='//button[@class="euiDatePopoverButton euiDatePopoverButton--start"]'
const data_refresh='//button[contains(.,"Refresh")]'



const uname_text='//input[@id="LoginUsernameTextField"]';



const password_text='//input[@id="LoginPasswordTextField"]';
const login_button='//button[@id="LoginButton"]'; 
let lengthcount, manualRecCount;
let reviewType,taskStatus,recordPresence;

module.exports = {
    commands: [
      {  
        getLoginInfo: function(environment){ 
        let i=0;         
        let env=data[i].Environment; 
        console.log("ENV",environment)         
        switch (environment)
        {
            case "DEV":
              return data2[i];
              break;
            case "UAT":
                return data2.UAT;
                break;
            case "Staging":
                return data2.STAGING;
                break;
            default:
                break;
        }        
      },
 
        loginkibana(){
          let kibanaurl=data2.DEV_URL;
          //let uname=data2.DEV_Username;
          //let password=data2.DEV_Password;            
          return this.api    // Open the Crome browser and navigate to the URL
		  
		  
          .url(kibanaurl)
          .maximizeWindow() 
		  
          .waitForElementVisible(searchbox_text,2000)
		            
          //.pause(2000)
        },
		searchKibana: function(){
          return this.api
          // waiting for the element to Load , In this case - searchbox in Kibana 
		  .waitForElementVisible(searchbox_text,3000)
		  .sendKeys(searchbox_text,data.EappNumber)
		  
		  //selecting the date for search kibana 
		  .click(date_parm)
		  .waitForElementVisible(date_parm2,3000)
		  .click(date_parm2)
		  .click(data_refresh)
		  
        },
              
},
],
};